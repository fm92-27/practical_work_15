﻿#include <iostream>
#include <string>

using namespace std;

class Person
{
public:
	Person()
	{
		rtng = 0;
	}
	Person(string _name, int _rtng) : name(_name), rtng(_rtng)
	{}
	string GetName()
	{
		return name;
	}
	int GetRtng()
	{
		return rtng;
	}
private:
	string name;
	int rtng;
};

void Conclusion(Person &arr, int n)
{
	Person *p = &arr;
	int count = n;
	bool b = true;

	while (b)
	{
		b = false;
		for (int i = 0; i + 1 < n; i++)
		{
			if (p[i].GetRtng() < p[i + 1].GetRtng())
			{
				swap(p[i + 1], p[i]);
				b = true;
			}
		}
	}

	for (int j = 0; j < n; j++)
	{
		cout << "Position " << j + 1;
		cout << " Name: " << p[j].GetName();
		cout << " Rating: " << p[j].GetRtng() << endl;
	}
}

void createArray(int n)
{
	string name;
	int rtng;
	Person *arr = new Person [n];

	for (int i = 0; i < n; i++)
	{
		cout << "Player " << i + 1 << endl;
		cout << "Name: ";
		cin >> name;
		cout << "Rating: ";
		cin >> rtng;
		arr[i] = Person(name, rtng);
		cout << endl;
	}

	Conclusion(*arr, n);
	delete[] arr;
}

int main()
{
	int n;

	cout << "Enter the number of players: ";
	cin >> n;
	cout << endl;

	createArray(n);
}
